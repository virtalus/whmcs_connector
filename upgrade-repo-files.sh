#!/bin/bash
cd /var/www/html/repo/hypercx-appliances

##--Packages--##
apt-ftparchive packages pool/ > dists/hypercx-appliances/main/binary-amd64/Packages

##--Packages.gz--##
gzip -kf dists/hypercx-appliances/main/binary-amd64/Packages

##--Release--##
apt-ftparchive release -c=aptftp.conf dists/hypercx-appliances/ > dists/hypercx-appliances/Release

##--Release.gpg--##
gpg --pinentry-mode=loopback --yes --passphrase "hypercx123" -u virtalus -bao dists/hypercx-appliances/Release.gpg dists/hypercx-appliances/Release

##--InRelease--##
gpg --yes -u virtalus --clear-sign --output dists/hypercx-appliances/InRelease dists/hypercx-appliances/Release

