## Zabbix Monitorization Configurations

This template is supported for Zabbix >= 4.2.

**Note:**
WHMCS must have the path `/var/www/whmcs` as the root of the web server.


* Install zabbix agent

```
wget https://repo.zabbix.com/zabbix/4.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.2-2+bionic_all.deb
dpkg -i zabbix-release_4.2-2+bionic_all.deb
apt update
apt install zabbix-agent
```

 * Configure the agent. For that, change the values of this lines (without brackets)

```
Server=[Zabbix server IP or Proxy IP]
ServerActive=[Zabbix server IP or Proxy IP]
Hostname=[WHMCS hostname]
```
**Note:**
If the WHMCS server is located on the same server where the Zabbix server is hosted, you must use the private IP of the Zabbix server. If the WHMCS server is hosted on a different server then the IP of the Zabbix Proxy container installed in the cluster must be used. The hostname must be the same that the hostname set in the Zabbix server.


### Set up MySQL monitoring

* Authenticate in the DB using `mysql -u root -p` and then enter the password
* Create the user `zabbix` using the cmd `CREATE USER 'zabbix' @ 'localhost' IDENTIFIED BY 'user_password';`
* Give all permissions in the DB to the user `zabbix` using `GRANT ALL PRIVILEGES ON *. * TO 'zabbix' @ 'localhost';`
* Create the file `.my.cnf` in the path `/var/lib/zabbix` and add the following lines:

```
[client]
user = "zabbix"
password = "password_configured"
```

* With the cmd `chmod o+rx` modify the permissions of the folders of the route and with the cmd `chmod o+r` modify the permissions of the file `.my.cnf`
* Restart the agent using `systemctl restart zabbix-agent`

**Note:**
If the path does not exist, create the necessary folders.Ensure that in the file `/etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf` the value of `HOME` is the path `/var/lib/zabbix`. That is the path where the `.my.cnf` file should be. You must be consistent with this if you want to change the location.


* Optionally adjust the permissions of the `authorized_key` file of the `root` user

```
chmod o+rx /root
chmod o+rx /root/.ssh
chmod o+r /root/.ssh/authorized_keys
```

* Install zabbix-sender package
```
apt install zabbix-sender
```
