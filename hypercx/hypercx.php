<?php

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
use WHMCS\Module\Server\HyperCX\MyMetricProvider;

require __DIR__."/lib/hypercx.php";
use WHMCS\Database\Capsule;

function hypercx_MetaData()
{
    return array(
        'DisplayName' => 'HyperCX',
        'APIVersion' => '1.1',
        'RequiresServer' => true,
        'DefaultNonSSLPort' => '1111',
        'DefaultSSLPort' => '1112',
        'ServiceSingleSignOnLabel' => 'Login to Panel as User',
        'ListAccountsUniqueIdentifierField'=> 'id',
        'AdminSingleSignOnLabel' => 'Login to Panel as Admin',
    );
}

function hypercx_MetricProvider($params){
    return new MyMetricProvider($params);
}

function hypercx_ConfigOptions()
{
  if (!Capsule::schema()->hasTable('tblhcxusers'))
    {
        Capsule::schema()->create('tblhcxusers', function ($table) {
            $table->string('serviceid');
            $table->string('userid');
            $table->integer('groupid');
        });

          logModuleCall(
              'hypercx',
              __FUNCTION__,
              $params,
              'unique',
              'Table not found and created');
    }

  return [
    "MemoryQuota" => [
        "FriendlyName" => "Default Memory Quota",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write value in GBs(don't write GBs itself)",
        "Default" => "10",
        ],
    "SystemDiskQuota" => [
        "FriendlyName" => "Default System Disk Quota",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write value in GBs(don't write GBs itself)",
        "Default" => "10",
        ],
    "CPUQuota" => [
        "FriendlyName" => "Default CPU Quota",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write value in cores(don't write cores itself)",
        "Default" => "10",
        ],
    "VMQuota" => [
        "FriendlyName" => "Default VM Quota",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write value in Numbers(don't write vms itself)",
        "Default" => "10",
        ],        
    "NetworkID" => [
        "FriendlyName" => "Enter Network ID",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write VN ID for next option of limit of leases",
        "Default" => "10",
        ],
    "NetworkLeases" => [
        "FriendlyName" => "Enter VN Quota",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write number of leases allowed for previously entered network ID",
        "Default" => "10",
        ],        
    "DatastoreID" => [
        "FriendlyName" => "Enter (Image)Datastore ID",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write ID for next option of limit of images and image size",
        "Default" => "10",
        ],
    "DatastoreImageNo" => [
        "FriendlyName" => "Enter Number of images allowed",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write number of images allowed for previously entered Datastore ID",
        "Default" => "10",
        ],        
    "DatastoreImageSize" => [
        "FriendlyName" => "Enter total size of images allowed",
        "Type" => "text", # Text Box
        "Size" => "25", # Defines the Field Width
        "Description" => "Write value in GBs(don't write GBs itself)",
        "Default" => "10",
        ],
    ];
}


function hypercx_CreateAccount(array $params)
{
    try {
        $result = hypercx::allocate_service($params);
        Capsule::table('tblhcxusers')->updateOrInsert(
            ['serviceid' => $params['serviceid']],
            [
              'groupid' => $result["HCX_GROUP_ID"],
              'userid' => $result["HCX_USER_ID"]
            ]
        );

      } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }
    return 'success';
}

function hypercx_SuspendAccount(array $params)
{
    try{
        $res = hypercx::change_password($params, "JE?Tdb.9Aygg7a&");
        if($res["success_func"] == -1){
            throw new \Exception("ERROR: Unable to change password: i = ".$res["ERROR"], 1);
        }
        hypercx::groupVMsAction($params);
    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }
    return 'success';
}

function hypercx_UnsuspendAccount(array $params)
{
    try{
      $res = hypercx::change_password($params, $params["customfields"]["Password"]);
      if($res["success_func"] == -1){
          throw new \Exception("ERROR: Unable to change password: i = ".$res["ERROR"], 1);
      }
    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

function hypercx_TerminateAccount(array $params)
{
    try{
      $res = hypercx::change_password($params, "eya,Bzm7uzWS}j7");
      if($res["success_func"] == -1){
        //   throw new \Exception("ERROR: Unable to change password: i = ".$res["ERROR"], 1);
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $res["ERROR"],
            $res["ERROR"]
        );
      }
      hypercx::deallocate_service($params);
    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}

function hypercx_ChangePassword(array $params)
{
    try {

    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}


function hypercx_ChangePackage(array $params)
{
    try {

    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return 'success';
}


function hypercx_TestConnection(array $params)
{
    try {
      switch(hypercx::test_connection($params['serverusername'],$params['serverpassword'],getUrl($params))){
        case 0:
            $success = false;
            $errorMsg = 'Server Connection timed out';
            break;
        case -1:
            $success = false;
            $errorMsg = 'Invalid credentials';
            break;
    	  case 1:
            $success = true;
            $errorMsg = 'Connected!';
            break;
        }
    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );
        $success = false;
        $errorMsg = $e->getMessage();
    }

    return array(
        'success' => $success,
        'error' => $errorMsg,
    );
}


function hypercx_AdminCustomButtonArray()
{
    return array(
        "Update Quota" => "updateQuota",
        "Product Default Quota" => "resetQuota",
        "No Quota" => "resetDefaultQuota",
        "Group ACL Fix" => "groupACLFix",
        "MarketPlaceApp ACL Fix" => "mpAppAclFix",
    );
}

function hypercx_ClientAreaCustomButtonArray()
{
    return array(
    );
}

function hypercx_updateQuota(array $params){
    try {
        $group_id = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
                                 ->value("groupid");
        
        hypercx::deleteOldQuotas($params, $group_id);
		$quotaParams = hypercx::getQuotaParams($params, hypercx::$MODE_UPDATE_SERVICE);
		$quotaParams["GroupID"] = $group_id;
        hypercx::updateQuota($params, $quotaParams);
        return "success";
    }
     catch (Exception $e) {
        return $e->getMessage();
    }
    
}


function hypercx_groupACLFix(array $params){
    try {
        $userID = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
                                 ->value("userid");
                                 
        $groupID = Capsule::table( 'tblhcxusers' )
        ->where('serviceid', $params['serviceid'])
        ->value("groupid");
        return hypercx::deleteGroupACL($params, $userID, $groupID);
    }
    catch (Exception $e) {
       return $e->getMessage();
   }
   
}

function hypercx_resetQuota(array $params){
    try {
        $group_id = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
                                 ->value("groupid");
        hypercx::deleteOldQuotas($params, $group_id);
		$quotaParams = hypercx::getQuotaParams($params, hypercx::$MODE_NEW_SERVICE);
		$quotaParams["GroupID"] = $group_id;
        hypercx::updateQuota($params, $quotaParams);
        return "success";
    }
     catch (Exception $e) {
        return $e->getMessage();
    }
    
}

function hypercx_resetDefaultQuota(array $params){
    try {
        $group_id = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
                                 ->value("groupid");
        hypercx::deleteOldQuotas($params, $group_id);
		$quotaParams = array(
            "MemoryQuota" => -1,
            "SystemDiskQuota" => -1,
            "VMQuota" => -1,
            "CPUQuota" => -1,
            "GroupID" => $group_id
        );
        hypercx::updateQuota($params, $quotaParams);
        return "success";
    }
     catch (Exception $e) {
        return $e->getMessage();
    }
    
}


function hypercx_mpAppAclFix(array $params){
    try {
        return hypercx::fixMarketplaceACL($params);
    }
     catch (Exception $e) {
        return $e->getMessage();
    }
}

function hypercx_AdminServicesTabFields(array $params)
{
    try {

        $response = array();
        $vms_str = "VMS:[";
        $vm = hypercx::get_vms($params);
        foreach($vm as $v){
          if(array_key_exists("ID",$v)){
            $vms_str = $vms_str." ".$v["ID"].":".strval(hypercx::get_single_vm($params, $v["ID"])["state"]);
          }
        }
        $vms_str = $vms_str."], TEMPLATES:[";
        $templates = hypercx::get_templates($params);
        if($templates["success"] >= 0){
          unset($templates["ERROR"]);
          unset($templates["success"]);
          foreach($templates as $temp){
            if(array_key_exists("ID",$temp)){
              $vms_str = $vms_str." ".$temp["ID"];
            }
          }
          $vms_str = $vms_str."], IMAGES: [";
        }
        else{
          $vms_str = $templates["ERROR"];
        }
        $images = hypercx::get_images($params);
        if($images["success"] >= 0){
          unset($images["ERROR"]);
          unset($images["success"]);
          foreach($images as $img){
          if(array_key_exists("ID",$img)){
              $vms_str = $vms_str." ".$img["ID"];
            }
          }
          $vms_str = $vms_str."]";
        }


        $group_id = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
                                 ->value("groupid");
        return array_merge(array(
            'Resources' => $vms_str,
            'Resources_ERROR' => $templates["ERROR"],
            'Password of the service' => $params["password"]
        ),hypercx::getGroupQuotasStr($params, $group_id));
    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

    }

    return array();
}

function hypercx_AdminServicesTabFieldsSave(array $params)
{

    $originalFieldValue = isset($_REQUEST['hypercx_original_uniquefieldname'])
        ? $_REQUEST['hypercx_original_uniquefieldname']
        : '';

    $newFieldValue = isset($_REQUEST['hypercx_uniquefieldname'])
        ? $_REQUEST['hypercx_uniquefieldname']
        : '';

    if ($originalFieldValue != $newFieldValue) {
        try {

        } catch (Exception $e) {

            logModuleCall(
                'hypercx',
                __FUNCTION__,
                $params,
                $e->getMessage(),
                $e->getTraceAsString()
            );


        }
    }
}

function hypercx_ServiceSingleSignOn(array $params)
{
    try {
        $response = array();

        return array(
            'success' => true,
            'redirectTo' => $response['redirectUrl'],
        );
    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return array(
            'success' => false,
            'errorMsg' => $e->getMessage(),
        );
    }
}


function hypercx_AdminSingleSignOn(array $params)
{
    try {
        // Call the service's single sign-on admin token retrieval function,
        // using the values provided by WHMCS in `$params`.
        $response = array();

        return array(
            'success' => true,
            'redirectTo' => $response['redirectUrl'],
        );
    } catch (Exception $e) {
        // Record the error in WHMCS's module log.
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return array(
            'success' => false,
            'errorMsg' => $e->getMessage(),
        );
    }
}

function hypercx_ClientArea(array $params)
{
    $status_ch_pass = 0;
    $requestedAction = isset($_REQUEST['customAction']) ? $_REQUEST['customAction'] : '';

    if ($requestedAction == 'manage') {
        $serviceAction = 'get_usage';
        $templateFile = 'templates/manage.tpl';
    }
    elseif($requestedAction == 'ChangePassword') {
        if(isset($_REQUEST['password1']) && isset($_REQUEST['password2'])){
          if($_REQUEST['password1'] == $_REQUEST['password2']){

            $ch_pass_res = hypercx::change_password($params, $_REQUEST['password2']);
            if($ch_pass_res["success_func"] == 1){
              $status_ch_pass = 1;
            }
            elseif($ch_pass_res["success_func"] == -403){
              $status_ch_pass = -404;
            }
            else{
              $status_ch_pass = -2;
            }
          }
          else{
            $status_ch_pass = -3;
          }
        }
        else{
          $status_ch_pass = -1;
        }
        $serviceAction = 'get_stats';
        $templateFile = 'templates/overview.tpl';
    }
     else {
        $serviceAction = 'get_stats';
        $templateFile = 'templates/overview.tpl';
    }

    try {

        $res = hypercx::get_vms($params);
        unset($res["success"]);
        return array(
            'tabOverviewReplacementTemplate' => $templateFile,
            'templateVariables' => array(
                'vm_info' => $res ,
                'HCX_CH_PASS' => $status_ch_pass ,
                'HCX_Output_Ch_pass' => $ch_pass_res ,
                'my_paramss' => $params ,
            ),
        );
    } catch (Exception $e) {
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return array(
            'tabOverviewReplacementTemplate' => 'error.tpl',
            'templateVariables' => array(
                'usefulErrorHelper' => $e->getMessage(),
            ),
        );
    }
}
