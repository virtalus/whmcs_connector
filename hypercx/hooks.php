<?php

function hook_hypercx_clientedit(array $params)
{
    try {
        // Call the service's function, using the values provided by WHMCS in
        // `$params`.
    } catch (Exception $e) {
        // Consider logging or reporting the error.
    }
}

add_hook('ClientEdit', 1, 'hook_hypercx_clientedit');

add_hook('ClientAreaPrimaryNavbar', 1, function ($menu)
{
    // Check whether the services menu exists.
    if (!is_null($menu->getChild('Services'))) {
        // Add a link to the module filter.
        $menu->getChild('Services')
            ->addChild(
                'Provisioning Module Products',
                array(
                    'uri' => 'clientarea.php?action=services&module=hypercx',
                    'order' => 15,
                )
            );
    }
});
