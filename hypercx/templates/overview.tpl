<h2>Overview</h2>

<p>HyperCX Overview Panel</p>
{IF $HCX_CH_PASS eq -1}
<div class="alert alert-danger">
<p>Something Went wrong</p>
</div>
{/IF}
{IF $HCX_CH_PASS eq -404}
<div class="alert alert-danger">
<p>Something Went wrong</p>
</div>
{/IF}
{IF $HCX_CH_PASS eq -3}
<div class="alert alert-danger">
<p>Passwords do not match</p>
</div>
{/IF}
{IF $HCX_CH_PASS eq 1}
<div class="alert alert-success">
<p>Password Changed successfully</p>
</div>
{/IF}

<div class="alert alert-info">
{if $moduleParams.serverhostname eq ""}
<a href=https:\\{$moduleParams.serverip}>Click here to access HyperCX portal</a>
{else}
<a href=https:\\{$moduleParams.serverhostname}>Click here to access HyperCX portal</a>
{/if}
</div>

<h3>{$LANG.clientareaproductdetails}</h3>

<hr>



{foreach from=$vm_info key=k item=v}
<div class="row">
    <div class="col-sm-5">

        VM # {$k+1}
    </div>
    <div class="col-sm-7">
        {$v.NAME}    <b>Status {$v.STATE} </b>
    </div>
</div>
{/foreach}


{if $lastupdate}
    <div class="row">
        <div class="col-sm-5">
            {$LANG.clientareadiskusage}
        </div>
        <div class="col-sm-7">
            {$diskusage}MB / {$disklimit}MB ({$diskpercent})
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            {$LANG.clientareabwusage}
        </div>
        <div class="col-sm-7">
            {$bwusage}MB / {$bwlimit}MB ({$bwpercent})
        </div>
    </div>
{/if}

<div class="row">
    <div class="col-sm-5">
        Username for HyperCX
    </div>
    <div class="col-sm-7">
        {$moduleParams.username}
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        {$LANG.firstpaymentamount}
    </div>
    <div class="col-sm-7">
        {$firstpaymentamount}
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        {$LANG.recurringamount}
    </div>
    <div class="col-sm-7">
        {$recurringamount}
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        {$LANG.clientareahostingnextduedate}
    </div>
    <div class="col-sm-7">
        {$nextduedate}
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        {$LANG.orderbillingcycle}
    </div>
    <div class="col-sm-7">
        {$billingcycle}
    </div>
</div>

<div class="row">
    <div class="col-sm-5">
        {$LANG.clientareastatus}
    </div>
    <div class="col-sm-7">
        {$status}
    </div>
</div>
<style>
table, th, td {
  border: 1px solid black;
  padding: 13px;
  text-align: left;
}
</style>
{if $suspendreason}
    <div class="row">
        <div class="col-sm-5">
            {$LANG.suspendreason}
        </div>
        <div class="col-sm-7">
            {$suspendreason}
        </div>
    </div>
{/if}
<table style="width:100%">
<tr>
    <th>Metric Name</th>
    <th>Usage</th>
    <th>Price</th>
</tr>

{foreach from=$metricStats item=v}
  <tr>
    <th>{$v.displayName}</th>
    <th>{$v.currentValue} </th>
    <th>
    {assign var=someVar value=" "|explode:$v.lowestPrice}
    {assign var=someVar value= $someVar.0|substr:1}
    {$someVar*$v.currentValue}
    </th>
</tr>

{/foreach}
</table>

<hr>

<div class="row">
    <div class="col-sm-4">
        <form method="post" action="clientarea.php?action=productdetails">
            <input type="hidden" name="id" value="{$serviceid}" />
            <input type="hidden" name="customAction" value="manage" />
            <button type="submit" class="btn btn-default btn-block">
                Change Password of HyperCX
            </button>
        </form>
    </div>

    {if $packagesupgrade}
        <div class="col-sm-4">
            <a href="upgrade.php?type=package&amp;id={$id}" class="btn btn-success btn-block">
                {$LANG.upgrade}
            </a>
        </div>
    {/if}

    <div class="col-sm-4">
        <a href="clientarea.php?action=cancel&amp;id={$id}" class="btn btn-danger btn-block{if $pendingcancellation}disabled{/if}">
            {if $pendingcancellation}
                {$LANG.cancellationrequested}
            {else}
                {$LANG.cancel}
            {/if}
        </a>
    </div>
</div>
