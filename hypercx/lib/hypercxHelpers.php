<?php

function humanReadableData($size, $mode="MB"){
    if($size < 0){
        return "INFINITE";
    }
    switch ($mode) {
        case 'MB':
            $size *= 1048576;
            break;
        case 'GB':
            $size *= 1073741824;
            break;
        case 'TB':
            $size *= 1099511627776;
            break;
        default:
            break;
    }
    if ($size / 1099511627776 > 1)
        return sprintf("%.2fTB", $size / 1099511627776);
    elseif ($size / 1073741824 > 1)
        return sprintf("%.2fGB", $size / 1073741824);
    elseif ($size / 1048576 > 1)
        return sprintf("%.2fMB", $size / 1048576);
    else
        return sprintf("%.2fKB", $size / 1024);
}
?>