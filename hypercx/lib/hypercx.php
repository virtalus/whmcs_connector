<?php

header('Content-type: text/html; charset=utf-8');

require __DIR__."/hypercxHelpers.php";

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
use WHMCS\Database\Capsule;

function getUrl($params){
	if($params["serversecure"]){
		return "https://".$params['serverhostname'].":". $params['serverport'] ."/RPC2";
	}
	else{
		return "http://".$params['serverip'].":". $params['serverport'] ."/RPC2";
	}
}

function getOneflowUrl($params){
	if($params["serversecure"]){
		return "https://".$params['serverhostname'].":2474";
	}
	else{
		return "http://".$params['serverip'].":2474";
	}
}

class hypercx{

	public static $MODE_NEW_SERVICE = 2041;
	public static $MODE_UPDATE_SERVICE = 2042;

	public static function test_connection($username,$password,$URL){
		
		$input_xml = '<?xml version="1.0" encoding="UTF-8"?>
					<methodCall>
						<methodName>one.userpool.info</methodName>
							<params>
								<param>
									<value><string>'.$username.':'.$password.'</string></value>
								</param>
							</params>
					</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL);
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		if(!$data){
				return 0;
		}
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		return $array_data['params']['param']['value']['array']['data']['value'][0]['boolean']?1:-1;
	}

  public static function allocate_service($params){
    $res = [];
    $res["HCX_GROUP_ID"] = -1;
    $params["HCX_USERNAME"] = str_replace("@",".",$params["clientsdetails"]["email"]);
    for($i=1;1;$i++){
       $res["HCX_GROUP_ID"] = hypercx::create_group($params);
       if(intval($res["HCX_GROUP_ID"]) < 1){
         $params["HCX_USERNAME"] = str_replace("@",$i.".",$params["clientsdetails"]["email"]);
       }
       else{
         break;
       }

       if($i > 10){
           throw new \Exception("ERROR: INFINITE LOOP ERROR: i = ".$res["HCX_GROUP_ID"], 1);
       }
	}
	hypercx::add_ACLRule($params, $res["HCX_GROUP_ID"]);
	hypercx::update_groupviews($params, $res["HCX_GROUP_ID"]);
	hypercx::fixMarketplaceACL($params);
	
	//Default quota setup
	$quotaParams = hypercx::getQuotaParams($params, hypercx::$MODE_NEW_SERVICE);
	$quotaParams["GroupID"] = $res["HCX_GROUP_ID"];
	hypercx::updateQuota($params, $quotaParams);
	
    $res["HCX_USER_ID"] = hypercx::create_user($params,$res["HCX_GROUP_ID"]);

    // $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
    $input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.group.addadmin</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
								<param>
									<value><i4>'.$res["HCX_GROUP_ID"].'</i4></value>
								</param>
                <param>
									<value><i4>'.$res["HCX_USER_ID"].'</i4></value>
								</param>
							</params>
					</methodCall>';

    $ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, getUrl($params));
  	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  	$data = curl_exec($ch);
  	curl_close($ch);

    if(!$data){
      throw new \Exception("ERROR:Occured in Allocate Service(Add Group Admin): Failed to Communicate to the server".$URL, 1);
    }
  	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
  	if(array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
		hypercx::delete_ACLRule_VN($params,$res["HCX_GROUP_ID"]);
		hypercx::deleteGroupACL($params, strval($res["HCX_USER_ID"]), strval($res["HCX_GROUP_ID"]));
  		return $res;
  	}
  	else{
      throw new \Exception("ERROR:Occured in Allocate Service(Add Group Admin): Failed to add group admin", 1);
	}
  }

  public static function deallocate_service($params){
    $vm = hypercx::get_vms($params);
    foreach($vm as $v){
      if(array_key_exists("ID",$v)){
        hypercx::vm_action($params, $v["ID"], 'terminate-hard');
      }
    }
    $vm_chk = true;
    while($vm_chk){
      $vm_chk = false;
      foreach($vm as $v){
        if(array_key_exists("ID",$v)){
          $cur_vm = hypercx::get_single_vm($params, $v["ID"]);
          if(intval($cur_vm["state"]) != 6){
            $vm_chk = true;
          }
        }
      }
	}
	
	$vrouters = hypercx::getVrouters($params);
    foreach($vrouters as $vrouter){
      if(array_key_exists("ID",$vrouter)){
        hypercx::vrouterDelete($params, $vrouter["ID"]);
      }
	}
	
	$services = hypercx::getServices($params);
	foreach($services as $service){
		if(array_key_exists("ID",$service)){
		  hypercx::serviceDelete($params, $service["ID"]);
		}
	  }

	$vnets = hypercx::getVnets($params);
    foreach($vnets as $vnet){
      if(array_key_exists("ID",$vnet)){
        hypercx::vnetDelete($params, $vnet["ID"]);
      }
    }
	
    $templates = hypercx::get_templates($params);
    if($templates["success"] >= 0){
      unset($templates["ERROR"]);
      unset($templates["success"]);
      foreach($templates as $temp){
        if(array_key_exists("ID",$temp)){
          hypercx::delete_template($params, $temp["ID"]);
        }
      }
    }
    sleep(10);
    $images = hypercx::get_images($params);
    if($images["success"] >= 0){
      unset($images["ERROR"]);
      unset($images["success"]);
      foreach($images as $img){
        if(array_key_exists("ID",$img)){
          hypercx::delete_image($params, $img["ID"]);
          logModuleCall(
            'hypercx',
            __FUNCTION__,
            $img["ID"],
            $img,
            $img);
        }
      }
	}
	
	$users = hypercx::getUsers($params);
    foreach($users as $user){
      if(array_key_exists("ID",$user)){
        hypercx::userDelete($params, $user["ID"]);
      }
	}
	
    sleep(10);

    $user_id = Capsule::table( 'tblhcxusers' )
  		 						 ->where('serviceid', $params['serviceid'])
  		 						 ->value("userid");
    $group_id =  Capsule::table( 'tblhcxusers' )
   		 						 ->where('serviceid', $params['serviceid'])
   		 						 ->value("groupid");

    $input_xml = '<?xml version="1.0"?>
          <methodCall>
            <methodName>one.user.delete</methodName>
              <params>
                <param>
                  <value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
                </param>
                <param>
                  <value><i4>'.$user_id.'</i4></value>
                </param>
              </params>
          </methodCall>';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getUrl($params));
    curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $data = curl_exec($ch);
    curl_close($ch);

    if(!$data){
        throw new \Exception("ERROR:Occured in Deallocate Service(User): Failed to Communicate to the server".getUrl($params), 1);
    }
    $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
    if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
    throw new \Exception("ERROR: Occured in Deallocate Service(User): ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
    }

    $input_xml = '<?xml version="1.0"?>
          <methodCall>
            <methodName>one.group.delete</methodName>
              <params>
                <param>
                  <value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
                </param>
                <param>
                  <value><i4>'.$group_id.'</i4></value>
                </param>
              </params>
          </methodCall>';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getUrl($params));
    curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    $data = curl_exec($ch);
    curl_close($ch);

    if(!$data){
        throw new \Exception("ERROR:Occured in Deallocate Service(Group): Failed to Communicate to the server".getUrl($params), 1);
    }
    $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
    if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
    throw new \Exception("ERROR: Occured in Deallocate Service(Group): ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
    }
  }
  

  public static function vm_action($params, $vm_id, $action){

    //   $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
      $input_xml = '<?xml version="1.0"?>
  					<methodCall>
  						<methodName>one.vm.action</methodName>
  							<params>
  								<param>
  									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
  								</param>
  								<param>
  									<value><string>'.$action.'</string></value>
  								</param>
  								<param>
  									<value><i4>'.$vm_id.'</i4></value>
  								</param>
  							</params>
  					</methodCall>';

      $ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, getUrl($params));
    	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    	$data = curl_exec($ch);
    	curl_close($ch);

      if(!$data){
          throw new \Exception("ERROR:Occured in VM Action: Failed to Communicate to the server".getUrl($params), 1);
      }
    	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
    	if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
  		throw new \Exception("ERROR: Occured in VM Action: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
    }
  }

  	public static function vnetDelete($params, $vnetID){
		  $input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.vn.delete</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'.$vnetID.'</i4></value>
									</param>
									<param>
										<value><boolean>1</boolean></value>
									</param>
								</params>
						</methodCall>';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);

		if(!$data){
			throw new \Exception("ERROR:Occured in VNET Delete: Failed to Communicate to the server".getUrl($params), 1);
		}
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
			throw new \Exception("ERROR: Occured in VNET Delete: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
		}
	}

  	public static function vrouterDelete($params, $vrouterID){
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.vrouter.delete</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'.$vrouterID.'</i4></value>
									</param>
									<param>
										<value><boolean>1</boolean></value>
									</param>
								</params>
						</methodCall>';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);

		if(!$data){
			throw new \Exception("ERROR:Occured in Vrouter Delete: Failed to Communicate to the server".getUrl($params), 1);
		}
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
			throw new \Exception("ERROR: Occured in Vrouter Delete: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
		}
  	}

	public static function serviceDelete($params, $serviceID){
		$cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, getOneflowUrl().'/service/'.$serviceID);
        curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($cURLConnection, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($cURLConnection, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cURLConnection, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($cURLConnection, CURLOPT_FAILONERROR, true);
        curl_setopt($cURLConnection, CURLOPT_USERPWD, $params["serverpassword"].":".$params["serverusername"]);  
    
        $servicesData = curl_exec($cURLConnection);
        curl_close($cURLConnection);
    
        if(intval(curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE)) != 204){
			return false;
        }
	}
	public static function userDelete($params, $userID){
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.user.delete</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'.$userID.'</i4></value>
									</param>
									<param>
										<value><boolean>1</boolean></value>
									</param>
								</params>
						</methodCall>';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);

		if(!$data){
			throw new \Exception("ERROR:Occured in User Delete: Failed to Communicate to the server".getUrl($params), 1);
		}
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
			throw new \Exception("ERROR: Occured in User Delete: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
		}
  	}
  
	public static function update_groupviews($params, $GID){
	// $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
    $input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.group.update</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
								<param>
									<value><i4>'.$GID.'</i4></value>
								</param>
								<param>
									<value><string>&lt;TEMPLATE&gt;&lt;SUNSTONE&gt;&lt;DEFAULT_VIEW&gt;&lt;![CDATA[simple]]&gt;&lt;/DEFAULT_VIEW&gt;&lt;GROUP_ADMIN_DEFAULT_VIEW&gt;&lt;![CDATA[advanced]]&gt;&lt;/GROUP_ADMIN_DEFAULT_VIEW&gt;&lt;GROUP_ADMIN_VIEWS&gt;&lt;![CDATA[simple,group_admin,advanced]]&gt;&lt;/GROUP_ADMIN_VIEWS&gt;&lt;VIEWS&gt;&lt;![CDATA[simple,advanced]]&gt;&lt;/VIEWS&gt;&lt;/SUNSTONE&gt;&lt;/TEMPLATE&gt;</string></value>
								</param>
								<param>
									<value><i4>1</i4></value>
								</param>
							</params>
					</methodCall>';

    $ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, getUrl($params));
  	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  	$data = curl_exec($ch);
  	curl_close($ch);

    if(!$data){
        throw new \Exception("ERROR:Occured in UPDATE VIEWS: Failed to Communicate to the server".getUrl($params), 1);
    }
  	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
  	if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
		throw new \Exception("ERROR:Occured in UPDATE VIEWS: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
  }
}
public static function delete_ACLRule_VN($params, $GID){
	// $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
	$group_hex = dechex(hexdec(200000000) + intval($GID));
	$input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.acl.info</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
							</params>
					</methodCall>';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, getUrl($params));
	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	$data = curl_exec($ch);
	curl_close($ch);

	if(!$data){
        throw new \Exception("ERROR:Occured in delete_ACLRule_VN: search: Failed to Communicate to the server".getUrl($params), 1);
	}
	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
	if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
		throw new \Exception("ERROR:Occured in delete_ACLRule_VN: search: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
	}
	$acl_list = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);
	if(array_key_exists("ACL",$acl_list)){
		foreach($acl_list["ACL"] as $acls){
			if($acls["USER"] == $group_hex && $acls["RESOURCE"] == '4400000000'){
				// $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
				$group_hex = dechex(hexdec(200000000) + intval($GID));
				$input_xml = '<?xml version="1.0"?>
								<methodCall>
									<methodName>one.acl.delrule</methodName>
										<params>
											<param>
												<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
											</param>
											<param>
												<value><i4>'.$acls["ID"].'</i4></value>
											</param>
										</params>
								</methodCall>';

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, getUrl($params));
				curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				$data = curl_exec($ch);
				curl_close($ch);

				if(!$data){
					throw new \Exception("ERROR:Occured in delete_ACLRule_VN: delete: Failed to Communicate to the server".getUrl($params), 1);
				}
				$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
				if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
					throw new \Exception("ERROR:Occured in delete_ACLRule_VN: delete: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
				}
				return "success";
			}
		}
		return "ACL rule has been already deleted. :)";
	}
}


public static function deleteGroupACL($params, $UID, $GID){
	$input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.acl.info</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
							</params>
					</methodCall>';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, getUrl($params));
	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	$data = curl_exec($ch);
	curl_close($ch);

	if(!$data){
        throw new \Exception("ERROR:Occured in deleteGroupACL: search: Failed to Communicate to the server".getUrl($params), 1);
	}
	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
	if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
		throw new \Exception("ERROR:Occured in deleteGroupACL: search: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
	}
	$acl_list = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);
	if(array_key_exists("ACL",$acl_list)){
		foreach($acl_list["ACL"] as $acls){
			if(strval($acls["STRING"]) == sprintf("#%s GROUP/#%s MANAGE *", $UID, $GID)){
				$input_xml = '<?xml version="1.0"?>
				<methodCall>
					<methodName>one.acl.delrule</methodName>
						<params>
							<param>
								<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
							</param>
							<param>
								<value><i4>'.$acls["ID"].'</i4></value>
							</param>
						</params>
				</methodCall>';

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, getUrl($params));
				curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				$data = curl_exec($ch);
				curl_close($ch);

				if(!$data){
					throw new \Exception("ERROR:Occured in deleteGroupACL: delete: Failed to Communicate to the server".getUrl($params), 1);
				}
				$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
				if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
					throw new \Exception("ERROR:Occured in deleteGroupACL: delete: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
				}
				return "success";
			}
		}
		return "ACL rule has been already deleted. :)";
	}
}
public static function getGroupQuotasStr($params, $GID){
	$vmQuotaOneKeys = array(
		"CPU" => "CPU Quota",
		"MEMORY" => "Memory Quota",
		"SYSTEM_DISK_SIZE" => "Disk Space Quota",
		"VMS" => "VM Quota",
	);
	$quotasArr = array();
	$input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.group.info</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
								<param>
									<value><i4>'.intval($GID).'</i4></value>
								</param>
							</params>
					</methodCall>';

    $ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, getUrl($params));
  	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  	$data = curl_exec($ch);
  	curl_close($ch);
	  
	if(!$data){
		throw new \Exception("ERROR:Occured in Fetching Group Quotas: Failed to Communicate to the server".getUrl($params), 1);
    }
	$groupDat = json_decode(json_encode(simplexml_load_string(str_replace("]]>","",str_replace("<![CDATA[","",json_decode(json_encode(simplexml_load_string($data)),true)['params']['param']['value']['array']['data']['value'][1]['string'])))),true);
	$quotasArr["VM QUOTAS"] = "";
	foreach($vmQuotaOneKeys as $oneKey => $whmcsKey){
		if(array_key_exists($oneKey ,$groupDat["VM_QUOTA"]["VM"])){
			if($oneKey == 'MEMORY' || $oneKey == 'RUNNING_MEMORY' || $oneKey == 'SYSTEM_DISK_SIZE'){
				$quotasArr["VM QUOTAS"] .= $whmcsKey."=".humanReadableData(floatval($groupDat["VM_QUOTA"]["VM"][$oneKey]));
			} else {
				if(floatval($groupDat["VM_QUOTA"]["VM"][$oneKey]) < 0) {
					$quotasArr["VM QUOTAS"] .= $whmcsKey."=INFINITE, ";
				} else {
					$quotasArr["VM QUOTAS"] .= $whmcsKey."=".$groupDat["VM_QUOTA"]["VM"][$oneKey];
				}
			}
			if($oneKey != "VMS"){
				$quotasArr["VM QUOTAS"] .= ", ";
			}
		}
	}
	$quotasArr["NETWORK QUOTAS"] = "";
	if(array_key_exists("ID", $groupDat["NETWORK_QUOTA"]["NETWORK"])){
		$quotasArr["NETWORK QUOTAS"] .= "VN ID=".$groupDat["NETWORK_QUOTA"]["NETWORK"]["ID"].", IP Quota=".$groupDat["NETWORK_QUOTA"]["NETWORK"]["LEASES"];
	} else {
		foreach($groupDat["NETWORK_QUOTA"]["NETWORK"] as $networkQuota){
			$quotasArr["NETWORK QUOTAS"] .= "VN ID=".$networkQuota["ID"].", IP Quota=".$networkQuota["LEASES"];
			break;
		}
	}
	$quotasArr["DATASTORE QUOTAS"] = "";
	if(array_key_exists("ID", $groupDat["DATASTORE_QUOTA"]["DATASTORE"])){
		$quotasArr["DATASTORE QUOTAS"] .= "(Image)Datastore ID=".$groupDat["DATASTORE_QUOTA"]["DATASTORE"]["ID"].", Number of Images=".$groupDat["DATASTORE_QUOTA"]["DATASTORE"]["IMAGES"].", Total Image Size=".humanReadableData(floatval($groupDat["DATASTORE_QUOTA"]["DATASTORE"]["SIZE"]));
	} else {
		foreach($groupDat["DATASTORE_QUOTA"]["DATASTORE"] as $datastoreQuota){
			$quotasArr["DATASTORE QUOTAS"] .= "(Image)Datastore ID=".$datastoreQuota["ID"].", Number of Images=".$datastoreQuota["IMAGES"].", Total Image Size=".humanReadableData(floatval($datastoreQuota["SIZE"]));
		}
	}
	return $quotasArr;
}
  public static function add_ACLRule($params, $GID){
	// $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
    $input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.acl.addrule</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
								<param>
									<value><string>'.dechex(hexdec(200000000) + intval($GID)).'</string></value>
								</param>
								<param>
									<value><string>'.dechex(hexdec("502d200000000") + intval($GID)).'</string></value>
								</param>
								<param>
									<value><string>8</string></value>
								</param>
							</params>
					</methodCall>';

    $ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, getUrl($params));
  	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  	$data = curl_exec($ch);
  	curl_close($ch);

    if(!$data){
        throw new \Exception("ERROR:Occured in A ADD ACL Rule: Failed to Communicate to the server".getUrl($params), 1);
    }
  	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
  	if(!array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
		throw new \Exception("ERROR:Occured in ADD ACL Rule: ".$ $array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
	}
}

public static function fixMarketplaceACL($params){
	$input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.acl.info</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
							</params>
					</methodCall>';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, getUrl($params));
	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	$data = curl_exec($ch);
	curl_close($ch);

	if(!$data){
        throw new \Exception("ERROR:Occured in fixMarketplaceACL: search: Failed to Communicate to the server".getUrl($params), 1);
	}
	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
	if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
		throw new \Exception("ERROR:Occured in fixMarketplaceACL: search: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
	}
	$acl_list = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);
	if(array_key_exists("ACL",$acl_list)){
		foreach($acl_list["ACL"] as $acls){
			if($acls["STRING"] == "* MARKETPLACE+MARKETPLACEAPP/* USE *"){
				$input_xml = '<?xml version="1.0"?>
								<methodCall>
									<methodName>one.acl.delrule</methodName>
										<params>
											<param>
												<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
											</param>
											<param>
												<value><i4>'.$acls["ID"].'</i4></value>
											</param>
										</params>
								</methodCall>';

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, getUrl($params));
				curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				$data = curl_exec($ch);
				curl_close($ch);

				if(!$data){
					throw new \Exception("ERROR:Occured in fixMarketplaceACL: delete: Failed to Communicate to the server".getUrl($params), 1);
				}
				$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
				if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
					throw new \Exception("ERROR:Occured in fixMarketplaceACL: delete: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
				}
				return "success";
			}
		}
		return "ACL Already deleted";
	}
}
  public static function create_group($params){
    // $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
    $input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.group.allocate</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
								<param>
									<value><string>'.$params["HCX_USERNAME"].'</string></value>
								</param>
							</params>
					</methodCall>';

    $ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, getUrl($params));
  	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  	$data = curl_exec($ch);
  	curl_close($ch);

    if(!$data){
        throw new \Exception("ERROR:Occured in Create Group: Failed to Communicate to the server".getUrl($params), 1);
    }
  	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
  	if(array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
        $input_xml = '<?xml version="1.0"?>
    					<methodCall>
    						<methodName>one.group.update</methodName>
    							<params>
                    <param>
                      <value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
                    </param>
    								<param>
    									<value><i4>'.$array_data['params']['param']['value']['array']['data']['value'][1]['i4'].'</i4></value>
    								</param>
    								<param>
    									<value><string>SID='.$params["serviceid"].'</string></value>
    								</param>
    							</params>
    					</methodCall>';

        $ch = curl_init();
      	curl_setopt($ch, CURLOPT_URL, getUrl($params));
      	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
      	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      	$data = curl_exec($ch);
      	curl_close($ch);

        if(!$data){
            throw new \Exception("ERROR:FAILED TO ADD SERVICE ID IN GROUP TEMPLATE: Failed to Communicate to the server".getUrl($params), 1);
        }
      	$array_data_new = json_decode(json_encode(simplexml_load_string($data)), true);
      	if(array_key_exists('i4', $array_data_new['params']['param']['value']['array']['data']['value'][1])){
          return intval($array_data['params']['param']['value']['array']['data']['value'][1]['i4']);
        }
        else{
          throw new \Exception("ERROR: FAILED TO ADD SERVICE ID IN GROUP TEMPLATE: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
        }
    }
  	else{
		  return -1;
    //   throw new \Exception("ERROR: Occured in Create Group: ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
  	}
  }

	public static function create_user($params, $GroupID){
    $params['model']->serviceProperties->save(['username' => $params['HCX_USERNAME']]);

    // $URL = "http://".$params["serverip"].":". $params['serverport'] ."/RPC2";
    $input_xml = '<?xml version="1.0"?>
					<methodCall>
						<methodName>one.user.allocate</methodName>
							<params>
								<param>
									<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
								</param>
								<param>
									<value><string>'.$params["HCX_USERNAME"].'</string></value>
								</param>
								<param>
									<value><string>'.$params["customfields"]["Password"].'</string></value>
								</param>
								<param>
									<value><string></string></value>
								</param>
								<param>
									<value>
										<array>
											<data>
												<value><i4>'.$GroupID.'</i4></value>
											</data>
										</array>
									</value>
								</param>
							</params>
					</methodCall>';

  	$ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, getUrl($params));
  	curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  	$data = curl_exec($ch);
  	curl_close($ch);

    if(!$data){
        throw new \Exception("ERROR:Occured in Create User: Failed to Communicate to the server".getUrl($params), 1);

    }
  	$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
  	if(array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
  		return intval($array_data['params']['param']['value']['array']['data']['value'][1]['i4']);
  	}
  	else{
  		throw new \Exception($array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
  	}
  }

  public static function convert_to_MB($inp, $type){
    switch ($type) {
      case 'GB':
        return $inp*1024;
        break;
      case 'TB':
        return $inp*1024*1024;
        break;
      default:
        return $inp*1024;
        break;
    }
  }
  public static function getQuotaParams($params, $mode){
	$quotaParams = array();
	switch ($mode){
		case hypercx::$MODE_NEW_SERVICE:
			$quotaParams["MemoryQuota"] = $params["configoption1"];
			$quotaParams["SystemDiskQuota"] = $params["configoption2"];
			$quotaParams["VMQuota"] = $params["configoption4"];
			$quotaParams["CPUQuota"] = $params["configoption3"];
			$quotaParams["VNID"] = $params["configoption5"];
			$quotaParams["VNQuota"] = $params["configoption6"];
			$quotaParams["DSID"] = $params["configoption7"];
			$quotaParams["DSQuotaNo"] = $params["configoption8"];
			$quotaParams["DSQuotaSize"] = $params["configoption9"];
			break;
		case hypercx::$MODE_UPDATE_SERVICE:
			$quotaParams["MemoryQuota"] = $params["customfields"]["Memory Quota"];
			$quotaParams["SystemDiskQuota"] = $params["customfields"]["Disk Space Quota"];
			$quotaParams["VMQuota"] = $params["customfields"]["VM Quota"];
			$quotaParams["CPUQuota"] = $params["customfields"]["CPU Quota"];
			$quotaParams["VNID"] = $params["customfields"]["VN ID"];
			$quotaParams["VNQuota"] = $params["customfields"]["IP Quota"];
			$quotaParams["DSID"] = $params["customfields"]["(Image)Datastore ID"];
			$quotaParams["DSQuotaNo"] = $params["customfields"]["Number of Images"];
			$quotaParams["DSQuotaSize"] = $params["customfields"]["Total Image Size"];
			break;
		default:
			break;
	}
	//MEMORYQUOTA
	if ($quotaParams["MemoryQuota"] == "" || !is_numeric($quotaParams["MemoryQuota"]) || (is_numeric($quotaParams["MemoryQuota"]) && (double)$quotaParams["MemoryQuota"] == 0)){
		$quotaParams["MemoryQuota"] = -2;
	}
	else{
		$quotaParams["MemoryQuota"] = hypercx::convert_to_MB((double)$quotaParams["MemoryQuota"], 'GB');
	}

	//SYSTEMDISKQUOTA
	if ($quotaParams["SystemDiskQuota"] == "" || !is_numeric($quotaParams["SystemDiskQuota"]) || (is_numeric($quotaParams["SystemDiskQuota"]) && (double)$quotaParams["SystemDiskQuota"] == 0)){
		$quotaParams["SystemDiskQuota"] = -2;
	}
	else{
		$quotaParams["SystemDiskQuota"] = hypercx::convert_to_MB((double)$quotaParams["SystemDiskQuota"], 'GB');
	}
	
	//CPUQUOTA
	if ($quotaParams["CPUQuota"] == "" || !is_numeric($quotaParams["CPUQuota"]) || (is_numeric($quotaParams["CPUQuota"]) && (double)$quotaParams["CPUQuota"] == 0)){
		$quotaParams["CPUQuota"] = -2;
	}

	//VMQUOTA
	if ($quotaParams["VMQuota"] == "" || !is_numeric($quotaParams["VMQuota"]) || (is_numeric($quotaParams["VMQuota"]) && (double)$quotaParams["VMQuota"] == 0)){
		$quotaParams["VMQuota"] = -2;
	}

	//VNID
	if ($quotaParams["VNID"] == "" || !is_numeric($quotaParams["VNID"]) || (is_numeric($quotaParams["VNID"]) && (double)$quotaParams["VNID"] == 0) || $quotaParams["VNQuota"] == "" || !is_numeric($quotaParams["VNQuota"]) || (is_numeric($quotaParams["VNQuota"]) && (double)$quotaParams["VNQuota"] == 0)){
		unset($quotaParams["VNID"]);
		unset($quotaParams["VNQuota"]);
	}

	//DS
	if (   $quotaParams["DSID"] == "" 
		|| !is_numeric($quotaParams["DSID"]) 
		|| (is_numeric($quotaParams["DSID"]) && (double)$quotaParams["DSID"] == 0) 
		|| $quotaParams["DSQuotaNo"] == "" 
		|| !is_numeric($quotaParams["DSQuotaNo"]) 
		|| (is_numeric($quotaParams["DSQuotaNo"]) 
		&& (double)$quotaParams["DSQuotaNo"] == 0)
		|| $quotaParams["DSQuotaSize"] == "" 
		|| !is_numeric($quotaParams["DSQuotaSize"]) 
		|| (is_numeric($quotaParams["DSQuotaSize"]) 
		&& (double)$quotaParams["DSQuotaSize"] == 0)
		){
		unset($quotaParams["DSID"]);
		unset($quotaParams["DSQuotaNo"]);
		unset($quotaParams["DSQuotaSize"]);
	} else {
		$quotaParams["DSQuotaSize"] = hypercx::convert_to_MB((double)$quotaParams["DSQuotaSize"], 'GB');
	}

	return $quotaParams;
  }

	public static function updateQuota($params, $quotaParams){	
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.group.quota</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'. $quotaParams["GroupID"] .'</i4></value>
									</param>
									<param>
										<value><string>VM=[MEMORY='.$quotaParams["MemoryQuota"].',RUNNING_MEMORY='.$quotaParams["MemoryQuota"].', SYSTEM_DISK_SIZE='.$quotaParams["SystemDiskQuota"].', VMS='.$quotaParams["VMQuota"].',RUNNING_VMS='.$quotaParams["VMQuota"].', CPU='.$quotaParams["CPUQuota"].', RUNNING_CPU='.$quotaParams["CPUQuota"].']</string></value>
									</param>
								</params>
						</methodCall>';
		logModuleCall(
		'hypercx',
		__FUNCTION__,
		$params,
		$quotaParams
			);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		$data = curl_exec($ch);
		curl_close($ch);

		if(!$data){
			throw new \Exception("ERROR:Occured in Update GroupQuota: Failed to Communicate to the server", 1);

		}
		//convert the XML result into array
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(intval($array_data['params']['param']['value']['array']['data']['value'][0]['boolean'])){
			if(array_key_exists("VNID", $quotaParams)){
				$input_xml = '<?xml version="1.0"?>
								<methodCall>
									<methodName>one.group.quota</methodName>
										<params>
											<param>
												<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
											</param>
											<param>
												<value><i4>'. $quotaParams["GroupID"] .'</i4></value>
											</param>
											<param>
												<value><string>NETWORK=[ID='.$quotaParams["VNID"].',LEASES='.$quotaParams["VNQuota"].']</string></value>
											</param>
										</params>
								</methodCall>';
								logModuleCall(
							'hypercx',
							__FUNCTION__,
							$params,
						$params
						);

				//setting the curl parameters.
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, getUrl($params));
				// Following line is compulsary to add as it is:
				curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
				$data = curl_exec($ch);
				curl_close($ch);
				if(!$data){
					throw new \Exception("ERROR:Occured in Update User NetworkQuota: Failed to Communicate to the server", 1);

				}
				//convert the XML result into array
				$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
				if(!intval($array_data['params']['param']['value']['array']['data']['value'][0]['boolean'])){
					throw new \Exception($array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
				}
			}
			if(array_key_exists("DSID", $quotaParams)){
				$input_xml = '<?xml version="1.0"?>
							<methodCall>
								<methodName>one.group.quota</methodName>
									<params>
										<param>
											<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
										</param>
										<param>
											<value><i4>'. $quotaParams["GroupID"] .'</i4></value>
										</param>
										<param>
											<value><string>DATASTORE=[ID='.$quotaParams["DSID"].',IMAGES='.$quotaParams["DSQuotaNo"].',SIZE='.$quotaParams["DSQuotaSize"].']</string></value>
										</param>
									</params>
							</methodCall>';
				logModuleCall(
					'hypercx',
					__FUNCTION__,
					$params,
					$params
				);

				//setting the curl parameters.
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, getUrl($params));
				// Following line is compulsary to add as it is:
				curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
				$data = curl_exec($ch);
				curl_close($ch);
				if(!$data){
				throw new \Exception("ERROR:Occured in Update User DatstoreQuota: Failed to Communicate to the server", 1);

				}
				//convert the XML result into array
				$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
				if(!intval($array_data['params']['param']['value']['array']['data']['value'][0]['boolean'])){
				throw new \Exception($array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
				}
			}
			return intval($array_data['params']['param']['value']['array']['data']['value'][0]['boolean']);
		}
		else{
			throw new \Exception('ERROR:UPDATE VM QUOTA: '.$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
		}
	}

	public static function deleteOldQuotas($params, $group_id){
		$groupQuotaVNIDs = hypercx::getNetworkQuotaIDs($params, $group_id);
		if($groupQuotaVNIDs["STATUS"]){
			$groupQuotaDat = array("GroupID" => $group_id);
			foreach ($groupQuotaVNIDs["ID"] as $VNID) {
				$groupQuotaDat["VNID"] = $VNID;
				hypercx::deleteNetworkQuota($params, $groupQuotaDat);
			}
		}

		$groupQuotaDSIDs = hypercx::getDSQuotaIDs($params, $group_id);
		if($groupQuotaDSIDs["STATUS"]){
			$groupQuotaDat = array("GroupID" => $group_id);
			foreach ($groupQuotaDSIDs["ID"] as $DSID) {
				$groupQuotaDat["DSID"] = $DSID;
				hypercx::deleteDSQuota($params, $groupQuotaDat);
			}
		}
	}

	public static function deleteNetworkQuota($params,$groupParams){
		//$URL = "http://".$params["serverip"].":2633/RPC2";
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.group.quota</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'. $groupParams["GroupID"] .'</i4></value>
									</param>
									<param>
										<value><string>NETWORK=[ID='.$groupParams["VNID"].']</string></value>
									</param>
								</params>
						</methodCall>';
		//setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		$data = curl_exec($ch);
		curl_close($ch);
		if(!$data){
			return false;
		}
		//convert the XML result into array
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!intval($array_data['params']['param']['value']['array']['data']['value'][0]['boolean'])){
			return false;
		}
		return true;
	}

	public static function getNetworkQuotaIDs($params, $groupID){
		$networkIDs = array();
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.group.info</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'. $groupID .'</i4></value>
									</param>
								</params>
						</methodCall>';
		//setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		$data = curl_exec($ch);
		curl_close($ch);
		if(!$data){
			$networkIDs["STATUS"] = false;
		} else {
			$networkIDs["STATUS"] = true;
			$networkIDs["ID"] = array();
			$groupNetworkQuota = json_decode(json_encode(simplexml_load_string(str_replace("]]>","",str_replace("<![CDATA[","",json_decode(json_encode(simplexml_load_string($data)),true)['params']['param']['value']['array']['data']['value'][1]['string'])))),true)["NETWORK_QUOTA"]["NETWORK"];
			if(is_array($groupNetworkQuota) && array_key_exists("ID",$groupNetworkQuota)){
				array_push($networkIDs["ID"],$groupNetworkQuota["ID"]);
			} elseif(is_array($groupNetworkQuota)) {
				foreach ($groupNetworkQuota as $networkQuota) {	
					array_push($networkIDs["ID"], $networkQuota["ID"]);
				}
			} else {
				$networkIDs["STATUS"] = false;
			}
		}
		return $networkIDs;
	}

	public static function deleteDSQuota($params,$groupParams){
		//$URL = "http://".$params["serverip"].":2633/RPC2";
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.group.quota</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'. $groupParams["GroupID"] .'</i4></value>
									</param>
									<param>
										<value><string>DATASTORE=[ID='.$groupParams["DSID"].']</string></value>
									</param>
								</params>
						</methodCall>';
		//setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		$data = curl_exec($ch);
		curl_close($ch);
		if(!$data){
			return false;
		}
		//convert the XML result into array
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!intval($array_data['params']['param']['value']['array']['data']['value'][0]['boolean'])){
			return false;
		}
		return true;
	}

	public static function getDSQuotaIDs($params, $groupID){
		$networkIDs = array();
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.group.info</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'. $groupID .'</i4></value>
									</param>
								</params>
						</methodCall>';
		//setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		$data = curl_exec($ch);
		curl_close($ch);
		if(!$data){
			$DSIDs["STATUS"] = false;
		} else {
			$DSIDs["STATUS"] = true;
			$DSIDs["ID"] = array();
			$groupDSQuota = json_decode(json_encode(simplexml_load_string(str_replace("]]>","",str_replace("<![CDATA[","",json_decode(json_encode(simplexml_load_string($data)),true)['params']['param']['value']['array']['data']['value'][1]['string'])))),true)["DATASTORE_QUOTA"]["DATASTORE"];
			if(is_array($groupDSQuota) && array_key_exists("ID",$groupDSQuota)){
				array_push($DSIDs["ID"],$groupDSQuota["ID"]);
			} elseif(is_array($groupDSQuota)) {
				foreach ($groupDSQuota as $DSQuota) {	
					array_push($DSIDs["ID"], $DSQuota["ID"]);
				}
			} else {
				$DSIDs["STATUS"] = false;
			}
		}
		return $DSIDs;
	}

  public static function delete_template($params, $template_id){

    // $URL = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.template.delete</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'.$template_id.'</i4></value>
									</param>
									<param>
										<value><boolean>1</boolean></value>
									</param>
								</params>
						</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		if(!$data){
      throw new \Exception("ERROR: DELETE TEMPLATE ID:".$template_id.": Failed to Communicate to Server. ", 1);
			return -1;
		}
    $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
    if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
      logModuleCall(
            'hypercx',
            __FUNCTION__,
            $data,
            $array_data['params']['param']['value']['array']['data']['value'],
            $array_data['params']['param']['value']['array']['data']['value']
        );
      throw new \Exception("ERROR: DELETE TEMPLATE ID:".$template_id.": ".var_dump($array_data['params']['param']['value']['array']['data']['value'][1]['string']), 1);
			return -1;
		}
    return 1;
  }

  public static function delete_image($params, $image_id){
    // $URL = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.image.delete</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'.$image_id.'</i4></value>
									</param>
								</params>
						</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
      throw new \Exception("ERROR: DELETE IMAGE ID:".$image_id.": Failed to Communicate to Server. ", 1);
			return -1;
		}
    $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
    if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
      throw new \Exception("ERROR: DELETE IMAGE ID:".$image_id.": ".$array_data['params']['param']['value']['array']['data']['value'][1]['string'], 1);
			return -1;
		}
    return 1;
  }

  public static function get_images($params){
    $group_id = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
								 ->value("groupid");
    if($group_id <= 0){
      $vm_states["success"] = -1;
			return $vm_states;
    }
    $images = array();
		// getUrl($params) = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.imagepool.info</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>-2</i4></value>
									</param>
									<param>
										<value><i4>-1</i4></value>
									</param>
									<param>
										<value><i4>-1</i4></value>
									</param>
								</params>
						</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
			$vm_states["success"] = 0;
      $vm_state["ERROR"] = "4";
			return $vm_states;
		}
    $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
    if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$templates["success"] = -1;
  		$templates["ERROR"] = $array_data['params']['param']['value']['array']['data']['value'][1]['string'];
			return $templates;
		}
    else{
      $image_pool = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);
  		$x=0;
  		if(array_key_exists("IMAGE",$image_pool)){
  			if(!array_key_exists("NAME", $image_pool["IMAGE"])){
  				foreach($image_pool["IMAGE"] as $imgs){
            $images["ERROR"] = "2";
            if($imgs["GID"] != $group_id){
              continue;
            }
            $images[$x] = array("ID" => $imgs["ID"],"NAME"=>$imgs["NAME"]);
  					$x = $x + 1;
  				}

  			}
  			else{
  				$images[0] =  array("ID" => $tmplts["IMAGE"]["ID"], "NAME"=>$tmplts["IMAGE"]["NAME"]);
  			}
  			$images["success"] = 1;

  		}
  		else{
  			$images["success"] = -2;
        $images["ERROR"] = "1";
  		}
  		return $images;
    }
  }

  public static function get_templates($params){
    $group_id = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
								 ->value("groupid");
    if($group_id <= 0){
      $vm_states["success"] = -1;
			return $vm_states;
    }
    $templates = array();
		// $URL = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.templatepool.info</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>-2</i4></value>
									</param>
									<param>
										<value><i4>-1</i4></value>
									</param>
									<param>
										<value><i4>-1</i4></value>
									</param>
								</params>
						</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
			$vm_states["success"] = 0;
      $vm_state["ERROR"] = "4";
			return $vm_states;
		}
    $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
    if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$templates["success"] = -1;
  		$templates["ERROR"] = $array_data['params']['param']['value']['array']['data']['value'][1]['string'];
			return $templates;
		}
    else{
      $template_pool = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);
  		$x=0;
  		if(array_key_exists("VMTEMPLATE",$template_pool)){
  			if(!array_key_exists("NAME", $template_pool["VMTEMPLATE"])){
  				foreach($template_pool["VMTEMPLATE"] as $tmplts){
            $templates["ERROR"] = "2";
            if($tmplts["GID"] != $group_id){
              continue;
            }
            $templates[$x] = array("ID" => $tmplts["ID"],"NAME"=>$tmplts["NAME"]);
  					$x = $x + 1;
  				}

  			}
  			else{
  				$templates[0] =  array("ID" => $tmplts["VMTEMPLATE"]["ID"], "NAME"=>$tmplts["VMTEMPLATE"]["NAME"]);
  			}
  			$templates["success"] = 1;

  		}
  		else{
  			$templates["success"] = -2;
        $templates["ERROR"] = "1";
  		}
  		return $templates;
    }
  }

  public static function get_single_vm($params, $vm_id){

    // $URL = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.vm.info</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'.$vm_id.'</i4></value>
									</param>
								</params>
						</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
			$vm_state["success"] = 0;
			return $vm_state;
		}
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$vm_state["success"] = -1;
  		$vm_state["ERROR"] = $array_data['params']['param']['value']['array']['data']['value'][1]['string'];
			return $vm_state;
		}
    else{
      $vm_state["success"] = 1;
      $vm_state["state"] = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true)["STATE"];
      logModuleCall(
            'hypercx',
            __FUNCTION__,
            json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true),
            json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true)["STATE"],
            json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true)["STATE"]
        );
			return $vm_state;
    }
  }

	public static function getVnets($params){
	$group_id = Capsule::table( 'tblhcxusers' )
									->where('serviceid', $params['serviceid'])
									->value("groupid");
		if($group_id <= 0){
		$vm_states["success"] = -1;
				return $vm_states;
		}
		
		$input_xml = '<?xml version="1.0"?>
							<methodCall>
								<methodName>one.vnpool.info</methodName>
									<params>
										<param>
											<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
										</param>
										<param>
											<value><i4>-2</i4></value>
										</param>
										<param>
											<value><i4>-2</i4></value>
										</param>
										<param>
											<value><i4>-1</i4></value>
										</param>
									</params>
							</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
			$vm_states["success"] = 0;
			return $vm_states;
		}
		
		$vnetPoolDat = array();
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$vnetPoolDat["success"] = -1;
			return $vnetPoolDat;
		}

		$vnetPoolArray = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);

		$x=0;
		if(array_key_exists("VNET",$vnetPoolArray)){
			if(!array_key_exists("NAME", $vnetPoolArray["VNET"])){
				foreach($vnetPoolArray["VNET"] as $vnet){
        			if($vnet["GID"] != $group_id){
            		continue;
          			}
					$vnetPoolDat[$x] = array("ID" => $vnet["ID"],"NAME"=>$vnet["NAME"]);
					$x = $x + 1;
				}
			}
			else{
				$vnetPoolDat[0] =  array("ID" => $vnets["VROUTER"]["ID"],"NAME"=>$vnets["VROUTER"]["NAME"]);
			}
			$vnetPoolDat["success"] = 1;
		}
		else{
			$vnetPoolDat["success"] = 2;
		}
		return $vnetPoolDat;
	}

	public static function getVrouters($params){
		$group_id = Capsule::table( 'tblhcxusers' )
									->where('serviceid', $params['serviceid'])
									->value("groupid");
		if($group_id <= 0){
		$vm_states["success"] = -1;
				return $vm_states;
		}
		
		$input_xml = '<?xml version="1.0"?>
							<methodCall>
								<methodName>one.vrouterpool.info</methodName>
									<params>
										<param>
											<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
										</param>
										<param>
											<value><i4>-2</i4></value>
										</param>
										<param>
											<value><i4>-2</i4></value>
										</param>
										<param>
											<value><i4>-1</i4></value>
										</param>
									</params>
							</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
			$vm_states["success"] = 0;
			return $vm_states;
		}
		
		$vroutersArr = array();
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$vroutersArr["success"] = -1;
			return $vroutersArr;
		}

		$vrouterData = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);

		$x=0;
		if(array_key_exists("VROUTER",$vrouterData)){
			if(!array_key_exists("NAME", $vrouterData["VROUTER"])){
				foreach($vrouterData["VROUTER"] as $vrouters){
        			if($vrouters["GID"] != $group_id){
            		continue;
          			}
					$vroutersArr[$x] = array("ID" => $vrouters["ID"],"NAME"=>$vrouters["NAME"]);
					$x = $x + 1;
				}
			}
			else{
				$vroutersArr[0] =  array("ID" => $vrouterData["VROUTER"]["ID"],"NAME"=>$vrouterData["VROUTER"]["NAME"]);
			}
			$vroutersArr["success"] = 1;
		}
		else{
			$vroutersArr["success"] = 2;
		}
		return $vroutersArr;
	}

	public static function getServices($params){
		$servicesArr = array();
		$group_id = Capsule::table( 'tblhcxusers' )
									->where('serviceid', $params['serviceid'])
									->value("groupid");
		if($group_id <= 0){
			$servicesArr["success"] = -1;
			return $servicesArr;
		}
		
		$serviceArr = array();
		$cURLConnection = curl_init();
		curl_setopt($cURLConnection, CURLOPT_URL, getOneflowUrl($params).'/service');
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($cURLConnection, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($cURLConnection, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($cURLConnection, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($cURLConnection, CURLOPT_FAILONERROR, true);
		curl_setopt($cURLConnection, CURLOPT_USERPWD, $params["serverusername"].$params["serverpassword"]);  

		$servicesData = curl_exec($cURLConnection);
		curl_close($cURLConnection);

		if(!$servicesData || intval(curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE)) != 200){
			$servicesArr["success"] = -1;
			return $servicesArr;
		}

		$servicesDataArr = json_decode($servicesData, true);

		$x=0;
		if(array_key_exists("DOCUMENT_POOL",$servicesDataArr)){
			if(array_key_exists("DOCUMENT",$servicesDataArr["DOCUMENT_POOL"])){
					if(!array_key_exists("NAME", $servicesDataArr["DOCUMENT_POOL"]["DOCUMENT"])){
						foreach ($servicesDataArr["DOCUMENT_POOL"]["DOCUMENT"] as $service) {
							if($service["GID"] == $group_id && $service["TEMPLATE"]["BODY"]["state"] != 5){
								$serviceArr[$x] = array("ID" => $service["ID"],"NAME"=>$service["NAME"]);
								$x++;
							}
						}
						$servicesArr["success"] = 1;
						return $serviceArr;
					} else {
						if(array_key_exists("GID", $servicesDataArr["DOCUMENT_POOL"]["DOCUMENT"]) && $servicesDataArr["DOCUMENT_POOL"]["DOCUMENT"]["GID"] == $group_id){
							$serviceArr[0] = array("ID" => $servicesDataArr["DOCUMENT_POOL"]["DOCUMENT"]["ID"],
							"NAME"=> $servicesDataArr["DOCUMENT_POOL"]["DOCUMENT"]["NAME"]
											);
							$servicesArr["success"] = 1;
							return $serviceArr;
						}
					}
			}
		}
		$servicesArr["success"] = -1;
		return $servicesArr;
	}

	public static function getUsers($params){
		$userDat = array();
		$group_id = Capsule::table( 'tblhcxusers' )
									->where('serviceid', $params['serviceid'])
									->value("groupid");
		if($group_id <= 0){
			$userDat["success"] = -1;
			return $userDat;
		}
		
		$input_xml = '<?xml version="1.0"?>
							<methodCall>
								<methodName>one.userpool.info</methodName>
									<params>
										<param>
											<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
										</param>
									</params>
							</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
			$UserDat["success"] = 0;
			return $UserDat;
		}
		
		$rawUserDat = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!$rawUserDat['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$userDat["success"] = -1;
			return $userDat;
		}

		$rawUserDat = json_decode(json_encode(simplexml_load_string($rawUserDat['params']['param']['value']['array']['data']['value'][1]['string'])), true);

		$x=0;
		if(array_key_exists("USER",$rawUserDat)){
			if(!array_key_exists("NAME", $rawUserDat["USER"])){
				foreach($rawUserDat["USER"] as $user){
        			if($user["GID"] == $group_id && $user["GNAME"] != $user["NAME"]){
						$userDat[$x] = array("ID" => $user["ID"],"NAME"=>$user["NAME"]);
						$x = $x + 1;
          			}
				}
			}
			else{
				$userDat[0] =  array("ID" => $rawUserDat["USER"]["ID"],"NAME"=>$rawUserDat["USER"]["NAME"]);
			}
			$userDat["success"] = 1;
		}
		else{
			$userDat["success"] = 2;
		}
		return $userDat;
	}

	public static function get_vms($params){

		$group_id = Capsule::table( 'tblhcxusers' )
									->where('serviceid', $params['serviceid'])
									->value("groupid");
		if($group_id <= 0){
		$vm_states["success"] = -1;
				return $vm_states;
		}
		// $URL = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.vmpool.info</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>-2</i4></value>
									</param>
									<param>
										<value><i4>-2</i4></value>
									</param>
									<param>
										<value><i4>-1</i4></value>
									</param>
									<param>
										<value><i4>-1</i4></value>
									</param>
									<param>
										<value><string></string></value>
									</param>
								</params>
						</methodCall>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();
		if(!$data){
			$vm_states["success"] = 0;
			return $vm_states;
		}
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!$array_data['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$vm_states["success"] = -1;
			return $vm_states;
		}

		$vm_data = json_decode(json_encode(simplexml_load_string($array_data['params']['param']['value']['array']['data']['value'][1]['string'])), true);

    	$vm_states = array();
		$x=0;
		if(array_key_exists("VM",$vm_data)){
			if(!array_key_exists("NAME", $vm_data["VM"])){
				foreach($vm_data["VM"] as $vms){
        			if($vms["GID"] != $group_id){
            		continue;
          			}
					$state_temp = '';
          			$state_code = -1;
					switch($vms["STATE"]){
						case 0:
							$state_temp = 'Initializing';
							break;
						case 1:
							$state_temp = 'Pending';
							break;
						case 2:
							$state_temp = 'Hold';
							break;
						case 3:
							$state_temp = 'Active';
							break;
						case 4:
							$state_temp = 'Stopped';
							break;
						case 5:
							$state_temp = 'Suspended';
							break;
						case 6:
							$state_temp = 'Done';
							break;
						case 8:
							$state_temp = 'Poweroff';
							break;
						case 9:
							$state_temp = 'Undeployed';
							break;
						case 10:
							$state_temp = 'Cloning';
							break;
						case 11:
							$state_temp = 'Cloning Failed';
							break;
					}
					$vm_states[$x] = array("ID" => $vms["ID"],"NAME"=>$vms["NAME"],'STATE'=>$state_temp, 'STATE_CODE' => $vms["STATE"]);
					$x = $x + 1;
				}
			}
			else{
				$vm_states[0] =  array("ID" => $vm_data["VM"]["ID"],"NAME"=>$vm_data["VM"]["NAME"],'STATE'=>$vm_data["VM"]["STATE"], 'STATE_CODE' => $vms["STATE"]);
			}
			$vm_states["success"] = 1;
		}
		else{
			$vm_states["success"] = 2;
		}
		return $vm_states;
	}

	public static function groupVMsAction($params, $action = 'poweroff-hard'){
		$vm = hypercx::get_vms($params);
		foreach($vm as $v){
			if(array_key_exists("ID",$v) && intval($v["STATE_CODE"]) == 3){
				hypercx::vm_action($params, $v["ID"], $action);
			}
		}
		$vm_chk = true;
		$start_time = time();
		while($vm_chk){
			if ((time() - $start_time) > 300) {
				
           throw new \Exception("ERROR: Unable to perform VM poweroff: Manually check what's blocking VM poweroff", 1); // timeout, function took longer than 5 minutes
			}
			$vm_chk = false;
			foreach($vm as $v){
				if(array_key_exists("ID",$v)){
					$cur_vm = hypercx::get_single_vm($params, $v["ID"]);
					if(intval($cur_vm["state"]) != 8){
						$vm_chk = true;
					}
				}
			}
		}
	}

	public static function change_password($params, $password){
		$userid = Capsule::table( 'tblhcxusers' )
								 ->where('serviceid', $params['serviceid'])
								 ->value("userid");
		// $URL = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
		$input_xml = '<?xml version="1.0"?>
						<methodCall>
							<methodName>one.user.passwd</methodName>
								<params>
									<param>
										<value><string>'.$params["serverusername"].':'.$params["serverpassword"].'</string></value>
									</param>
									<param>
										<value><i4>'.strval($userid).'</i4></value>
									</param>
									<param>
										<value><string>'.$password.'</string></value>
									</param>
								</params>
						</methodCall>';


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);

		if(!$data){
      return array(
				"success_func" => -1,
				"Error" => "Couldn't communicate with the server"
			);
		}
		$array_data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(array_key_exists('i4', $array_data['params']['param']['value']['array']['data']['value'][1])){
			return array(
				"success_func" => 1,
				"Error" => $array_data['params']['param']['value']['array']['data']['value'][1]['i4']
			);
		}
		else{
			logModuleCall(
			'hypercx',
			__FUNCTION__,
			strval($userid).":".$password,
			$array_data['params']['param']['value']['array']['data']['value'][1]['string'],
			$array_data['params']['param']['value']['array']['data']['value'][1]['string']
		);
		return array(
			"userid" => $userid,
			"success_func" => -403,
			"Error" => $array_data['params']['param']['value']['array']['data']['value'][1]['string']
		);
		}
	}

  public static function send_xml_request($params){
    $input_xml = $params['input_xml'];
    // $URL = 'http://'.$params["serverip"].':'. $params["serverport"] .'/RPC2';
    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, getUrl($params));
		// Following line is compulsary to add as it is:
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input_xml);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		curl_close($ch);
		$params = array();

    return $data;
  }

	public static function check_connection($data){
		$data = json_decode(json_encode(simplexml_load_string($data)), true);
		if(!$data['params']['param']['value']['array']['data']['value'][0]['boolean']){
			$params["success"] = -1;
			$params["error"] = $data['params']['param']['value']['array']['data']['value'][1]['string'];
			return $params;
		}
		else{
			$params["success"] = 1;
			$params["error"] = 'Success!';
			return $params;
		}
	}
}
?>
