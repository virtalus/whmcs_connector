<?php
namespace WHMCS\Module\Server\HyperCX;
use WHMCS\Database\Capsule;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use WHMCS\UsageBilling\Contracts\Metrics\MetricInterface;
use WHMCS\UsageBilling\Contracts\Metrics\ProviderInterface;
use WHMCS\UsageBilling\Metrics\Metric;
use WHMCS\UsageBilling\Metrics\Units\GigaBytes;
use WHMCS\UsageBilling\Metrics\Units\WholeNumber;
use WHMCS\UsageBilling\Metrics\Units\FloatingPoint;
use WHMCS\UsageBilling\Metrics\Usage;

class MyMetricProvider implements ProviderInterface
{
    private $moduleParams = [];
    public function __construct($moduleParams)
    {
        $this->moduleParams = $moduleParams;
    }

    private function getUrl(){
      if($this->moduleParams["serversecure"]){
        return "https://".$this->moduleParams['serverhostname'].":". $this->moduleParams['serverport'] ."/RPC2";
      }
      else{
        return "http://".$this->moduleParams['serverip'].":". $this->moduleParams['serverport'] ."/RPC2";
      }
    }


    public function metrics()
    {
        return [
            new Metric(
                'hcx_cpu_used',
                'CPU core Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new WholeNumber("cpus")
            ),

            new Metric(
                'hcx_vm',
                'VM amount Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new FloatingPoint("vms","vm","vms","","vms")
            ),

            new Metric(
                'hcx_image_size',
                'Image Gigabyte Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new GigaBytes()
            ),

            new Metric(
                'hcx_ssd_used',
                'SSD Gigabyte Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new GigaBytes()
            ),

            new Metric(
                'hcx_hdd_used',
                'HDD Gigabyte Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new GigaBytes()
            ),

            new Metric(
                'hcx_ram',
                'RAM Gigabyte Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new GigaBytes()
            ),

            new Metric(
                'hcx_public_ip',
                'Public IP amount Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new FloatingPoint("ips","ip","ips","","ips")
            ),
            
            new Metric(
                'hcx_cpu_core',
                'CPU Core Days',
                MetricInterface::TYPE_PERIOD_MONTH,
                new FloatingPoint("cpucorehours","core","cores","","cores")
            )
        ];
    }

    public function usage()
    {
        $serverData = $this->apiCall('stats');
        $usage = [];
        foreach ($serverData as $data) {
            $usage[$data["username"]] = $this->wrapUserData($data);
        }
          logModuleCall(
              'UsageTest',
              __FUNCTION__,
              $tenant,
              $usage,
              $usage
          );

        return $usage;
    }

    public function tenantUsage($tenant)
    {
        return $this->wrapUserData($this->apiCall($tenant));
    }

    private function wrapUserData($data)
    {
        $wrapped = [];
        foreach ($this->metrics() as $metric) {
            $key = $metric->systemName();
            if ($data[$key]) {
                $value = $data[$key];
                $metric = $metric->withUsage(
                    new Usage($value)
                  );
            }

            $wrapped[] = $metric;
        }

        return $wrapped;
    }
    private function get_curr_usage($tenant, $metric){
      //select tbltenant_stats.metric, tblserver_tenants.tenant, tbltenant_stats.value  from tbltenant_stats INNER JOIN tblserver_tenants ON tblserver_tenants.ID = tbltenant_stats.tenant_id WHERE tblserver_tenants.tenant = "kamran.virtalus.com";
      try {
          $usage = Capsule::table('tbltenant_stats')
               ->join(
                   'tblserver_tenants',
                   'tblserver_tenants.id',
                        '=', 'tbltenant_stats.tenant_id'
               )
              ->where(
                'tblserver_tenants.tenant',"=",$tenant
                )
              ->where(
                'tbltenant_stats.metric','=',$metric
                )
              ->value("tbltenant_stats.value");
          if(!$usage){
            return 0;
          }
          return $usage;
      } catch(\Illuminate\Database\QueryException $ex){
          return 0;
      } catch (Exception $e) {
          return 0;
      }
    }

    private function apiCall($action)
    {
      try{
        $usage_dat = [];
        if($action == "stats"){
          $server_dat = system("cd /var/www/whmcs/modules/servers/hypercx/lib && ruby hypercx-acct-whcms -u ".$this->moduleParams["serverusername"]." -p ".$this->moduleParams["serverpassword"]." -e '".$this->getUrl()."'");
          $server_dat = json_decode($server_dat, true);

          logModuleCall(
              'UsageTest',
              __FUNCTION__,
              $action,
              $server_dat,
              $server_dat
          );
          foreach($server_dat as $dat) {
            $usage_dat_individual = [];
            foreach ($dat as $key => $newusage) {
              if($key != "username"){
                if($key == "hcx_cpus_used"){
                  $usage_dat_individual[$key] = intval($this->get_curr_usage(strval($dat["username"]), strval($key))) + round($newusage / 24);
                }
                else{
                  $usage_dat_individual[$key] = floatval($this->get_curr_usage(strval($dat["username"]), strval($key))) + (floatval($newusage) / 24);
                }
              }
            }
            $usage_dat_individual["username"] = $dat["username"];
            $usage_dat[] = $usage_dat_individual;
          }
          logModuleCall(
              'UsageTest',
              __FUNCTION__,
              $server_dat,
              $usage_dat,
              $usage_dat
          );
          return $usage_dat;
        }
        else{
          $usage_dat = [];
          $server_dat = system("cd /var/www/whmcs/modules/servers/hypercx/lib && ruby hypercx-acct-whcms -u ".$this->moduleParams["serverusername"]." -p ".$this->moduleParams["serverpassword"]." -e '".$this->getUrl()."' -g ".$action."");
          $server_dat = json_decode($server_dat, true);
          foreach ($server_dat as $key => $newusage) {
              $usage_dat[$key] = $this->get_curr_usage(strval($action), strval($key));
          }
          logModuleCall(
              'UsageTest',
              __FUNCTION__,
              $action,
              $usage_dat,
              $usage_dat
          );
        }
      }
      catch(Exception $e){
        logModuleCall(
            'hypercx',
            __FUNCTION__,
            __CLASS__ ,
            $e->getMessage(),
            $e->getTraceAsString()
        );
      }
        return $usage_dat;
      
    }
}
?>
