# WHMCS Connector for HyperCX

## Overview

HyperCX Bento Public clouds rely on WHMCS for billing and registering new users. The connector is actively maintained by Virtalus and it's objective is to provide seamless integration between HyperCX and WHMCS. It features automatic provisioning of virtualization services as well deprovisioning services on account termination, WHMCS invoices clients based on what they use per day. Billing is achieved using Usage Metrics feature of WHMCS, although some modifications are made to support per day billing. 

HyperCX leverages a fork of OpenNebula where the APIs are not modified, so this connector comes with full compatibility with OpenNebula clouds.

![](../picts/services.png)

![](../picts/detailed_service_censored.jpg)

## Behaviour 

All allocations/deallocations and automation is performed using XML-RPC and performed as oneadmin. Upon first service purchase from the public cloud a database table is created in WHMCS's database which stores group id, groupadmin id of client against client's Service ID, for rest of the services same table will be utilized.

### Provisioning and Deprovisioning

#### Service Creation

Once a user purchases a service(which is on zero upfront), a group and a group admin is allocated with user's desired password while username will be client's email address with `@` replaced a `.`, although if a user decides to add a new service to same account a trailing integer will be added to the username before `@`(eg for email john@abc.com, username would be john.abc.com, and second service would have john1.abc.com and so on). Required ACL rule will be added in order to allow clients to create templates,image and virtual networks. Group ID and Groupadmin ID is stored in the database for future references.

#### Service Suspension

Upon service suspension client's data will remain intact and only password is changed of the group admin. If the client's account is unsuspended, password will be reverted the password provided by the user to WHMCS(regardless of what user sets the password using sunstone WHMCS will always revert to the password provided to WHMCS either on account creation or using change password from clientarea)

#### Service Termination

Upon service termination every resource associated to the group will be recursively deleted which includes Virtual machines, Virtual Machine Templates and Images. Database entry of client's Group ID and Groupadmin ID is also deleted on WHMCS Server. 

### Stats and control on WHMCS

- For clients WHMCS can provide with accurate usage statistics, as well as an estimate of client's usage cost.
- List of VMs and their states.
- Client can change password of the groupadmin from client are of WHMCS.
- Control total number of clients on a cluster depending upon cluster's capacity.

`Note: WHMCS variable pricing is supported on invoices, but will not be accurately reflected at Client Area`

### Billing

Billing is the most complicated part, since WHMCS utilizes Usage Metrics for billing according usage of resources. But as of writing this Readme WHMCS supports monthly billing only and usage data is only collected twice a day by default. So we customized this process by adding a custom cron job which runs hourly collects data,calculates corresponding values and updates accordingly. This custom modification utilizes DB read for getting current values of metrics and adds up the current data after dividing it by 24.

Example

1st hour: 1 VM / 24  = 0.0415 + previous summation(i.e. 0)

2nd hour: 2 VM / 24  = 0.0833 + previous summation(i.e. 0.0415)

and so on .. 
following this sequence an average is determined based on the data collected for all hours of the day instead of two intervals per day(default WHMCS)

### Installation
`Ubuntu is assumed however these steps can be reproduced for other distributions as well`
- Clone this repository and copy hypercx directory to *WHMCS_DIR*/modules/servers/
- Now add hypercx key, repository to apt and install `opennebula-rubygems && ruby-opennebula`.(If this step is skipped Metrics won't work)
- Add crontab for hourly monitoring(run `crontab -l | { cat; echo "@hourly php -q /var/www/whmcs/crons/cron.php do --TenantUsageMetrics"; } | crontab -` )

### Configuration

- Create a new server group and add a server to WHMCS, use module HyperCX. Required attributes are Server IP, username and password.(must be oneadmin!)
- Before saving server configuration run `Test Connection`.
- Create a new product group and a product. Select HyperCX module while creating the new product and then select the server group previously created in module settings.
-  Add BILL_TYPE attribute for system datastores and public networks on Sunstone
    - For Datastores use BILL_TYPE = 'SSD' *or* 'HDD' (skipping this step would result in faulty metric values)
    - For Public networks use BILL_TYPE = 'PUBLIC' 
Note: Rest configurations are upto the user, also don't forget to enable all metric settings wherever necessary. Follow [WHMCS Documentation](https://docs.whmcs.com/Documentation_Home/) to setup desired configurations

### Monitoring with Zabbix

Monitoring of this connector and related services is under development phase and Zabbix stack is decided to be used. Intended use of zabbix would be via bash command of zabbix_sender by PHP.
